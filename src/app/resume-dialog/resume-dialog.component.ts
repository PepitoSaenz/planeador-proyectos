import { Component, ViewChild, OnInit, Inject, HostListener } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";

import * as XLSX from "xlsx";



@Component({
  selector: 'app-resume-dialog',
  templateUrl: './resume-dialog.component.html',
  styleUrls: ['./resume-dialog.component.scss']
})
export class ResumeDialogComponent implements OnInit {

  @ViewChild('resumeTable') table: any;

  ingesta;
  process;
  summed_values = []
  total_values = []

  constructor(private dialogRef: MatDialogRef<ResumeDialogComponent>, @Inject(MAT_DIALOG_DATA) public datas: any, public snackBar: MatSnackBar) { 
    console.log(datas);

    this.summed_values = datas[0];
    this.total_values = datas[1];
    this.ingesta = datas[0]["ingesta"];
    this.process = datas[0]["procesamiento"];
  }

  ngAfterViewChecked():void {
    window.dispatchEvent(new Event('resize'));
    }

  ngOnInit(): void {
    window.dispatchEvent(new Event('resize'));
  }

  exportResume() {
    console.log("EXPORT");
    this.openSnackBar("Resumen Exportado", "Ok");
    this.exportExcelResume(this.prepareExportData());
  }

  //Exporta y descarga una serie de datos en formato JSON a un archivo de Excel.
  exportExcelResume(json: any[]): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {
      Sheets: { "Resumen": worksheet },
      SheetNames: ["Resumen"],
    };
    XLSX.writeFile(workbook, `Resumen_Planeador_${new Date().getTime()}.xlsx`);
  }

  //Devuelve el color de la fila si este es generado
  getRowClass(row: any) {
    if (row["proceso"].includes("Total")) 
      return { finalTotalRow: true };
  }

  closeDialog() {
    this.dialogRef.close(null);
  }

  getCellClass(row){
    return { 'centered-field': true };
  }

  toggleGroup(group: any) {
    this.table.groupHeader.toggleExpandGroup(group);
  }

  onDetailToggle(event) {
    //console.log('Detail Toggled', event);
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 9000;
    if (action == "Ok") {
      config.panelClass = ["sucessMessage"];
    } else if (action == "Warning") {
      config.panelClass = ["warnningMessage"];
    } else {
      config.panelClass = ["errorMessage"];
    }
    this.snackBar.open(message, action, config);
  }

  prepareExportData() {
    let exportData = [];
    let tmpTotal = {
      "Grupo": "TOTAL HORAS",
      "Proceso": "TOTAL HORAS PROYECTO",
      "Cantidad de Tablas/Jobs": "",
      "Horas de Proceso": this.summed_values["proyecto"]
    };
    let tmpTotalData = {
      "Grupo": "TOTAL HORAS",
      "Proceso": "TOTAL HORAS PROYECTO DATA",
      "Cantidad de Tablas/Jobs": "",
      "Horas de Proceso": this.summed_values["proyecto_data"]
    };
    let tmpTotalOtros = {
      "Grupo": "TOTAL HORAS",
      "Proceso": "TOTAL HORAS OTRAS ACTIVIDADES",
      "Cantidad de Tablas/Jobs": "",
      "Horas de Proceso": this.summed_values["otros"]
    };

    exportData.push(tmpTotal);
    exportData.push(tmpTotalData);
    exportData.push(tmpTotalOtros);

    for (let row of this.total_values) {
      let tmp = {
        "Grupo": row["processGroup"],
        "Proceso": row["proceso"],
        "Cantidad de Tablas/Jobs": row["cantidad"],
        "Horas de Proceso": row["junior"]
      }
      exportData.push(tmp);
    }

    return exportData;
  }

}
