
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";

import { ResumeDialogComponent } from "./resume-dialog/resume-dialog.component";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(public snackBar: MatSnackBar, public dialog: MatDialog) {

  }

  ngAfterViewChecked(): void {
    window.dispatchEvent(new Event('resize'));
  }

  ngOnInit(): void {
    console.log("INIT PLANEADOR jeje")
    window.dispatchEvent(new Event('resize'));
  }

  editing = {};
  editingGob = {};
  editingMiniGob = {};

  loadingIndicator = true;
  reorderable = true;

  activeHelp: string = "";


  calcGobRow(row: any) {
    if (parseInt(row["tablas"]) < parseInt(row["A"]) + parseInt(row["M"]) + parseInt(row["B"])) {
      row["senior"] = "Error";
      row["junior"] = "Error"
    } else {
      let level = String(this.calcComplexLevelGob(row));
      row["senior"] = this.calcComplejidadGob(row, "A", level)[0] + this.calcComplejidadGob(row, "M", level)[0] + this.calcComplejidadGob(row, "B", level)[0];
      row["junior"] = this.calcComplejidadGob(row, "A", level)[1] + this.calcComplejidadGob(row, "M", level)[1] + this.calcComplejidadGob(row, "B", level)[1];
    }
    this.calcTotalGob();
  }

  calcComplexLevelGob(row: any) {
    for (let level of this.constant_complex_levels)
      if (level["level"] < 4 && row["campos"] >= level["min"] && row["campos"] <= level["max"])
        return level["level"];
    return "4";
  }

  calcComplejidadGob(row: any, property: string, level: string) {
    const values = this.constants_gob[property + level];
    return [(row[property] * values["tiempos_senior"]), (row[property] * values["tiempos_junior"])];
  }


  //Calcula el total de valores para la tabla de procesamiento de Gobierno
  calcTotalGob() {
    const len = this.tables_values.length - 1;

    this.tables_values[len]["tablas"] = this.tables_values[len]["campos"] = this.tables_values[len]["A"] = this.tables_values[len]["M"] =
      this.tables_values[len]["B"] = this.tables_values[len]["senior"] = this.tables_values[len]["junior"] =
      this.mini_table_values[0]["tablas"] = this.mini_table_values[0]["senior"] = 0;

    for (let pos = 0; pos < len; pos++) {
      this.tables_values[len]["tablas"] += this.tables_values[pos]["tablas"];
      this.tables_values[len]["campos"] += this.tables_values[pos]["campos"];
      this.tables_values[len]["A"] += this.tables_values[pos]["A"];
      this.tables_values[len]["M"] += this.tables_values[pos]["M"];
      this.tables_values[len]["B"] += this.tables_values[pos]["B"];
      this.mini_table_values[0]["tablas"] += this.tables_values[pos]["tablas"];
      if (String(this.tables_values[pos]["senior"]) != "Error") {
        this.tables_values[len]["senior"] += this.tables_values[pos]["senior"];
        this.tables_values[len]["junior"] += this.tables_values[pos]["junior"];
      }
    }
    this.mini_table_values[0]["senior"] = this.mini_table_values[0]["tablas"] * 4;

    this.mini_table_values[1]["tablas"] = this.mini_table_values[0]["tablas"];
    this.mini_table_values[1]["senior"] = this.mini_table_values[0]["senior"];
  }

  addRowGob(rowIndex: number) {
    let newRow = { "pos": 0, "campos": 0, "tablas": 0, "A": 0, "M": 0, "B": 0, "senior": 0, "junior": 0 };

    this.tables_values.splice(rowIndex + 1, 0, newRow);
    this.tables_values = [...this.tables_values];
    for (let pos = 0; pos < this.tables_values.length - 1; pos++)
      this.tables_values[pos]["pos"] = pos;

    this.openSnackBar("Fila creada", "Ok");
  }

  removeRowGob(rowIndex: number) {
    this.tables_values.splice(rowIndex, 1);
    this.tables_values = [...this.tables_values];

    for (let pos = 0; pos < this.tables_values.length - 1; pos++)
      this.tables_values[pos]["pos"] = pos;

    this.calcTotalGob();
    this.openSnackBar("Fila eliminada", "Ok");
  }




  calcIngestaRow(row: any, activity: string) {
    if (parseInt(row["tablas"]) < parseInt(row["A"]) + parseInt(row["M"]) + parseInt(row["B"]))
      row["tiempos"] = "Error"
    else
      row["tiempos"] = this.calcComplejidad("A", activity, row) + this.calcComplejidad("M", activity, row) + this.calcComplejidad("B", activity, row);

    row["senior"] = (row["tiempos"] == "Error" ? "Error" : row["tiempos"] / 2);
    row["junior"] = row["tiempos"];

    this.calcTotalIngesta();
  }

  calcComplejidad(type: string, activity: string, row: any) {
    if (row[type])
      return this.constants[type][activity] * row[type];
    else
      return 0;
  }

  //Calcula el total de valores para la tabla de procesamiento de Gobierno
  calcTotalIngesta() {
    const lensub = this.ingesta_values.length - 2;
    let subRow = this.ingesta_values[lensub];

    const len = this.ingesta_values.length - 1;
    let totalRow = this.ingesta_values[len];

    subRow["tablas_tot"] =  0;
    for (let pos = 0; pos < lensub; pos++)
      subRow["tablas_tot"] += this.ingesta_values[pos]["tablas"];

    subRow["A"] = subRow["tablas_tot"] > 100 ? 1 : 0;
    subRow["M"] = 50 < subRow["tablas_tot"] && subRow["tablas_tot"] < 301 ? 1 : 0;
    subRow["B"] = subRow["tablas_tot"] < 51 ? 1 : 0;

    subRow["junior"] = this.calcComplejidad("A", "Despliegue", subRow) 
      + this.calcComplejidad("M", "Despliegue", subRow) + this.calcComplejidad("B", "Despliegue", subRow);
    subRow["senior"] = subRow["junior"] / 2;

    totalRow["tablas"] = totalRow["A"] = totalRow["M"] = totalRow["B"] = totalRow["tiempos"] = totalRow["senior"] = totalRow["junior"] = 0
    for (let pos = 0; pos < lensub; pos++)
      if (String(this.ingesta_values[pos]["tiempos"]) != "Error") {
        totalRow["tablas"] += this.ingesta_values[pos]["tablas"];
        totalRow["A"] += this.ingesta_values[pos]["A"];
        totalRow["M"] += this.ingesta_values[pos]["M"];
        totalRow["B"] += this.ingesta_values[pos]["B"];
        totalRow["tiempos"] += this.ingesta_values[pos]["tiempos"];
        totalRow["senior"] += this.ingesta_values[pos]["senior"];
        totalRow["junior"] += this.ingesta_values[pos]["junior"];
      }
    
    if (totalRow["tiempos"] > 0) 
      totalRow["senior"] += subRow["senior"];
      totalRow["junior"] += subRow["junior"];
    
    delete subRow["A"];
    delete subRow["M"];
    delete subRow["B"];
  }

  //Abre el popup de los comentarios
  showResumeDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.height = "90%";
    dialogConfig.width = "75%";
    dialogConfig.disableClose = false;
    dialogConfig.data = this.calcResumeDataIngesta();
    this.dialog.open(ResumeDialogComponent, dialogConfig);
  }

  calcResumeDataIngesta() {
    const len = this.tables_values.length - 1;
    const lening = this.ingesta_values.length - 1;
    const lenproc = this.procesamiento_values.length - 1;
    const lenproc_sub = 3;

    let total_constantes_proc = 152;
    let total_consts = this.ingesta_values[lening]["tablas"] > 0 ? total_constantes_proc : 0;

    let total_ingesta = this.tables_values[len]["junior"] + this.ingesta_values[lening]["junior"] + total_consts;
    let total_procesamiento = this.procesamiento_values[lenproc]["tiempos"] + (this.procesamiento_values[lenproc]["tablas"] > 0 ? total_constantes_proc : 0);
    let total_otros = this.mini_table_values[0]["senior"] + (this.ingesta_values[lening]["tablas"] > 0 ? 64 : 0) + (this.procesamiento_values[lenproc]["tablas"] > 0 ? 24 : 0);
    let total_data = total_ingesta + total_procesamiento - total_otros;

    let totales = {
      "ingesta": total_ingesta,
      "procesamiento": total_procesamiento,
      "proyecto": total_ingesta + total_procesamiento,
      "proyecto_data": total_data,
      "otros": total_otros
    };

    let datos = [
      {
        check: true,
        processGroup: "INGESTA",
        proceso: "Gobierno Tecnico",
        cantidad: this.tables_values[len]["tablas"],
        horas: this.tables_values[len]["senior"],
        junior: this.tables_values[len]["junior"]
      },
      {
        processGroup: "INGESTA",
        proceso: "Arquitectura y Seguridad",
        cantidad: this.mini_table_values[0]["tablas"],
        horas: this.mini_table_values[0]["senior"],
        junior: this.mini_table_values[0]["senior"]

      },
      {
        processGroup: "INGESTA",
        proceso: "Total Horas Gobierno",
        horas: this.tables_values[len]["senior"] + this.mini_table_values[0]["senior"],
        junior: this.tables_values[len]["junior"]
      },
      {
        check: true,
        processGroup: "INGESTA",
        proceso: "Data X",
        cantidad: this.ingesta_values[0]["tablas"],
        horas: this.ingesta_values[0]["tiempos"],
        junior: this.ingesta_values[0]["junior"]
      },
      {
        check: true,
        processGroup: "INGESTA",
        proceso: "Automatizacion Control M Dist",
        cantidad: this.ingesta_values[1]["tablas"],
        horas: this.ingesta_values[1]["tiempos"],
        junior: this.ingesta_values[1]["junior"]
      },
      {
        processGroup: "INGESTA",
        proceso: "MSPD",
        cantidad: this.ingesta_values[2]["tablas"],
        horas: this.ingesta_values[2]["tiempos"],
        junior: this.ingesta_values[2]["junior"]
      },
      {
        check: true,
        processGroup: "INGESTA",
        proceso: "Desarrollo Ingestas Nuevas/Modificadas",
        cantidad: this.ingesta_values[3]["tablas"],
        horas: this.ingesta_values[3]["tiempos"],
        junior: this.ingesta_values[3]["junior"]
      },
      {
        check: true,
        processGroup: "INGESTA",
        proceso: "Hammurabi MVP",
        cantidad: this.ingesta_values[4]["tablas"],
        horas: this.ingesta_values[4]["tiempos"],
        junior: this.ingesta_values[4]["junior"]
      },
      {
        check: true,
        processGroup: "INGESTA",
        proceso: "Extracción Archivos",
        cantidad: this.ingesta_values[5]["tablas"],
        horas: this.ingesta_values[5]["tiempos"],
        junior: this.ingesta_values[5]["junior"]
      },
      {
        processGroup: "INGESTA",
        proceso: "Total Horas (Analisis, Desarrollo y Pruebas)",
        horas: this.ingesta_values[lening]["tiempos"],
        junior: this.ingesta_values[lening]["junior"],
      },
      {
        processGroup: "INGESTA",
        proceso: "Total horas Certificacion e Implantacion",
        junior: total_consts
      },
      {
        processGroup: "PROCESAMIENTOS",
        proceso: "Procesamientos de Alta Complejidad",
        cantidad: this.procesamiento_values[lenproc]["A"],
        junior: (this.procesamiento_values[lenproc_sub]["A"] * this.constants["A"]["Procesamiento"]) + (this.procesamiento_values[lenproc - 1]["A"] * this.constants["A"]["Data X"])
      },
      {
        processGroup: "PROCESAMIENTOS",
        proceso: "Procesamientos de Media Complejidad",
        cantidad: this.procesamiento_values[lenproc]["M"],
        junior: (this.procesamiento_values[lenproc_sub]["M"] * this.constants["M"]["Procesamiento"]) + (this.procesamiento_values[lenproc - 1]["M"] * this.constants["M"]["Data X"])
      },
      {
        processGroup: "PROCESAMIENTOS",
        proceso: "Procesamientos de Baja Complejidad",
        cantidad: this.procesamiento_values[lenproc]["B"],
        junior: (this.procesamiento_values[lenproc_sub]["B"] * this.constants["B"]["Procesamiento"]) + (this.procesamiento_values[lenproc - 1]["B"] * this.constants["B"]["Data X"])
      },
      {
        processGroup: "PROCESAMIENTOS",
        proceso: "Total Horas (Analisis, Desarrollo y Pruebas)",
        cantidad: this.procesamiento_values[lenproc]["tablas"],
        junior: this.procesamiento_values[lenproc]["tiempos"],
      },
      {
        processGroup: "PROCESAMIENTOS",
        proceso: "Total horas Certificacion e Implantacion",
        junior: this.procesamiento_values[lenproc]["tablas"] > 0 ? total_constantes_proc : 0
      }
    ]

    return [totales, datos];
  }

  getRowClassGob(row: any) {
    if (parseInt(row["pos"]) % 2 == 1) {
      return { "intercell-grey": false };
    } else if (row["pos"] < 0)
      return { "total-fields": true };
  }

  getCellClassGob(row: any) {
    if (String(row.row["campos"]) != "Totales") {
      return { "editable-fields": true };
    }
  }

  getRowClassIngesta(row: any) {
    if (row["actividad"])
      if (row["actividad"].includes("Total Horas"))
        return { "total-fields": true };
    if (row["actividad"].includes("Documentación"))
      return { "subtotal-fields": true };
  }

  //Devuelve el color de la fila si este es generado
  getCellClassIng(row: any) {
    if (row.row["actividad"])
      if (!row.row["actividad"].includes("Total Horas") && !row.row["actividad"].includes("SubTotal"))
        return { "editable-fields": true };
  }

  getRowClassProcess(row: any) {
    if (row["proceso"]) {
      if (row["proceso"].includes("Total Horas"))
        return { "total-fields": true };
      if (row["proceso"].includes("SubTotal"))
        return { "subtotal-fields": true };
    }

    if (row["pink"] && row["edit"])
      return { "editable-pink-field": true };
    if (row["pink"])
      return { "subtotal-fields": true };
  }

  getCellClassProcess(row: any) {
    return { "text-aligned-center": true };
  }

  getCellClassProcessEdit(row: any) {
    if (row.row["proceso"])
      if (!row.row["proceso"].includes("Total Horas"))
        return { "editable-fields": true };
    if (row.row["proceso"].length == 0)
      return { "editable-fields": true };
  }

  getDisabledCellClass(row: any) {
    return { "disabled-fields": true };
  }

  getTitleClass(row: any) {
    return { "title-fields": true };
  }

  updateValueGob(event, cell, rowIndex) {

    this.editingGob[rowIndex + '-' + cell] = false;
    this.tables_values[rowIndex][cell] = parseInt(event.target.value);
    this.tables_values = [...this.tables_values];
    this.calcGobRow(this.tables_values[rowIndex]);
  }


  updateValue(event, cell, rowIndex) {

    this.editing[rowIndex + '-' + cell] = false;
    this.ingesta_values[rowIndex][cell] = parseInt(event.target.value);
    this.ingesta_values = [...this.ingesta_values];
    this.calcIngestaRow(this.ingesta_values[rowIndex], this.ingesta_values[rowIndex]["actividad"]);
  }

  updateValueProcess(event, cell, rowIndex) {
    this.procesamiento_values[rowIndex][cell] = parseInt(event.target.value);
    this.procesamiento_values = [...this.procesamiento_values];

    this.calcProcessRow(this.procesamiento_values[rowIndex]);
  }

  calcProcessRow(row: any) {
    

    if (parseInt(row["tablas"]) < parseInt(row["A"]) + parseInt(row["M"]) + parseInt(row["B"])) {
      row["tiempos"] = "Error"
    } else {
      if (row["proceso"] == "Data X") {
        row["tiempos"] = this.calcComplejidad("A", "Data X", row)
          + this.calcComplejidad("M", "Data X", row) + this.calcComplejidad("B", "Data X", row);
      } else {
        row["tiempos"] = this.calcComplejidad("A", "Procesamiento", row)
          + this.calcComplejidad("M", "Procesamiento", row) + this.calcComplejidad("B", "Procesamiento", row);
      }
    }
    this.calcTotalProcess();
  }

  calcTotalProcess(){
    const len = this.procesamiento_values.length - 1;
    const lenMid = 3;
    const lenDatax = this.procesamiento_values.length - 2;

    this.procesamiento_values[len]["tiempos"] = (String(this.procesamiento_values[lenMid]["tiempos"]) != "Error" ? this.procesamiento_values[lenMid]["tiempos"] : 0)
      + (String(this.procesamiento_values[lenDatax]["tiempos"]) != "Error" ? this.procesamiento_values[lenDatax]["tiempos"] : 0);
    this.procesamiento_values[len]["tablas"] = (String(this.procesamiento_values[lenMid]["tiempos"]) != "Error" ? this.procesamiento_values[lenMid]["tablas"] : 0)
      + (String(this.procesamiento_values[lenDatax]["tiempos"]) != "Error" ? this.procesamiento_values[lenDatax]["tablas"] : 0);

    this.procesamiento_values[len]["A"] = (String(this.procesamiento_values[lenMid]["tiempos"]) != "Error" ? this.procesamiento_values[lenMid]["A"] : 0)
      + (String(this.procesamiento_values[lenDatax]["tiempos"]) != "Error" ? this.procesamiento_values[lenDatax]["A"] : 0);
    this.procesamiento_values[len]["M"] = (String(this.procesamiento_values[lenMid]["tiempos"]) != "Error" ? this.procesamiento_values[lenMid]["M"] : 0)
      + (String(this.procesamiento_values[lenDatax]["tiempos"]) != "Error" ? this.procesamiento_values[lenDatax]["M"] : 0);
    this.procesamiento_values[len]["B"] = (String(this.procesamiento_values[lenMid]["tiempos"]) != "Error" ? this.procesamiento_values[lenMid]["B"] : 0)
      + (String(this.procesamiento_values[lenDatax]["tiempos"]) != "Error" ? this.procesamiento_values[lenDatax]["B"] : 0);
  }

  getRowHeight(row) {
    if (row["edit"])
      return 50;
    else if (row["proceso"].includes("Total"))
      return 60;
    else
      return 35;
  }


  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 9000;
    if (action == "Ok") {
      config.panelClass = ["sucessMessage"];
    } else if (action == "Warning") {
      config.panelClass = ["warnningMessage"];
    } else {
      config.panelClass = ["errorMessage"];
    }
    this.snackBar.open(message, action, config);
  }

  columns = [{ prop: 'name' }, { name: 'Gender' }, { name: 'Company', sortable: false }];

  mini_table_values = [
    {
      "campos": "VoBo Arquitectura",
      "tablas": 0,
      "senior": 0
    },
    {
      "campos": "VoBo Seguridad",
      "tablas": 0,
      "senior": 0
    }
  ]

  tables_values = [
    {
      "pos": 0,
      "campos": 0,
      "tablas": 0,
      "A": 0,
      "M": 0,
      "B": 0,
      "senior": 0,
      "junior": 0
    },
    {
      "pos": 1,
      "campos": 0,
      "tablas": 0,
      "A": 0,
      "M": 0,
      "B": 0,
      "senior": 0,
      "junior": 0
    },
    {
      "pos": -1,
      "campos": 0,
      "tablas": 0,
      "A": 0,
      "M": 0,
      "B": 0,
      "senior": 0,
      "junior": 0
    }
  ]

  procesamiento_values = [
    {
      "pink": true,
      "proceso": "MSPD"
    },
    {
      "pink": true,
      "proceso": "DT (Mapeo y Diagrama)"
    },
    {
      "pink": true,
      "proceso": "Construcción Procesamiento"
    },
    {
      "pink": true,
      "edit": true,
      "proceso": "Despliegues Work / Pruebas",
      "procesamientos:": 0,
      "tablas": 0,
      "A": 0,
      "M": 0,
      "B": 0,
      "tiempos": 0
    },
    {
      "pink": true,
      "proceso": "Test unitario, TAT"
    },
    {
      "pink": true,
      "proceso": "Modificación y/o creación de Malla"
    },
    {
      "pink": true,
      "proceso": "Pruebas integrales (Procesamiento, Control-M)"
    },
    {
      "pink": true,
      "proceso": "Documentación Certificación"
    },
    {
      "edit": true,
      "proceso": "Data X",
      "procesamientos:": 0,
      "tablas": 0,
      "A": 0,
      "M": 0,
      "B": 0,
      "tiempos": 0
    },
    {
      "total": true,
      "proceso": "Total Horas (Analisis, Desarrollo y Pruebas)",
      "tablas": 0,
      "A": 0,
      "M": 0,
      "B": 0,
      "tiempos": 0
    }
  ]

  ingesta_values = [
    {
      "actividad": "Data X",
      "tablas": 0,
      "A": 0,
      "M": 0,
      "B": 0,
      "tiempos": 0,
      "senior": 0,
      "junior": 0,
    },
    {
      "actividad": "Automatización Control M Dist",
      "tablas": 0,
      "A": 0,
      "M": 0,
      "B": 0,
      "tiempos": 0,
      "senior": 0,
      "junior": 0,
    },
    {
      "actividad": "MSPD",
      "tablas": 0,
      "A": 0,
      "M": 0,
      "B": 0,
      "tiempos": 0,
      "senior": 0,
      "junior": 0,
    },
    {
      "actividad": "Desarollo Ingestas Nuevas/Modificadas",
      "tablas": 0,
      "A": 0,
      "M": 0,
      "B": 0,
      "tiempos": 0,
      "senior": 0,
      "junior": 0,
    },
    {
      "actividad": "Hammurabi MVP",
      "tablas": 0,
      "A": 0,
      "M": 0,
      "B": 0,
      "tiempos": 0,
      "senior": 0,
      "junior": 0,
    },
    {
      "actividad": "Extracción Archivos Workload",
      "tablas": 0,
      "A": 0,
      "M": 0,
      "B": 0,
      "tiempos": 0,
      "senior": 0,
      "junior": 0,
    },
    {
      "actividad": "Documentación Certificación",
      "senior": 0,
      "junior": 0,
      "total": true
    },
    {
      "actividad": "Total Horas (Analisis, Desarrollo y Pruebas)",
      "tablas": 0,
      "A": 0,
      "M": 0,
      "B": 0,
      "tiempos": 0,
      "senior": 0,
      "junior": 0,
      "total": true
    }
  ];

  constant_complex_levels = [
    {
      "level": 1,
      "min": 1,
      "max": 40
    },
    {
      "level": 2,
      "min": 41,
      "max": 80
    },
    {
      "level": 3,
      "min": 81,
      "max": 120
    },
    {
      "level": 4,
      "min": 121,
      "max": 240
    }
  ]

  constants_gob = {
    "B1": {
      "tiempos_senior": 8,
      "tiempos_junior": 12
    },
    "B2": {
      "tiempos_senior": 16,
      "tiempos_junior": 24
    },
    "B3": {
      "tiempos_senior": 24,
      "tiempos_junior": 36
    },
    "B4": {
      "tiempos_senior": 64,
      "tiempos_junior": 84
    },
    "M1": {
      "tiempos_senior": 12,
      "tiempos_junior": 18
    },
    "M2": {
      "tiempos_senior": 24,
      "tiempos_junior": 36
    },
    "M3": {
      "tiempos_senior": 48,
      "tiempos_junior": 72
    },
    "M4": {
      "tiempos_senior": 60,
      "tiempos_junior": 90
    },
    "A1": {
      "tiempos_senior": 20,
      "tiempos_junior": 30
    },
    "A2": {
      "tiempos_senior": 40,
      "tiempos_junior": 60
    },
    "A3": {
      "tiempos_senior": 60,
      "tiempos_junior": 90
    },
    "A4": {
      "tiempos_senior": 80,
      "tiempos_junior": 120
    }
  }



  constants = {
    "A": {
      "Data X": 3,
      "Automatización Control M Dist": 3,
      "MSPD": 1,
      "Desarollo Ingestas Nuevas/Modificadas": 8,
      "Hammurabi MVP": 5,
      "Despliegue": 32,
      "Procesamiento": 160,
      "Extracción Archivos Workload": 3
    },
    "M": {
      "Data X": 2,
      "Automatización Control M Dist": 2,
      "MSPD": 1,
      "Desarollo Ingestas Nuevas/Modificadas": 4,
      "Hammurabi MVP": 3,
      "Despliegue": 24,
      "Procesamiento": 120,
      "Extracción Archivos Workload": 2
    },
    "B": {
      "Data X": 1,
      "Automatización Control M Dist": 1,
      "MSPD": 1,
      "Desarollo Ingestas Nuevas/Modificadas": 1,
      "Hammurabi MVP": 1,
      "Despliegue": 16,
      "Procesamiento": 80,
      "Extracción Archivos Workload": 1
    }
  };

  recargar() {
    console.log("hola")
  }

  constants_toshow = [
    {
      actividad: "Changelog",
      horas: 24,
      pan: true
    },
    {
      actividad: "Catalogador",
      horas: 24,
      pan: true
    },
    {
      actividad: "VoBo MX Control M Distribuido",
      horas: 40,
      groups: true
    },
    {
      actividad: "Certificación (Calidad Banco)",
      horas: 24,
      phone: true
    },
    {
      actividad: "Despliegue e Implantación Live",
      horas: 24,
      groups: true
    },
    {
      actividad: "Post-Implantación",
      horas: 16,
      contact: true
    },
    {
      actividad: "TOTAL",
      horas: 152
    }
  ]

}
