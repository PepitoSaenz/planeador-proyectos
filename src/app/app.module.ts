import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from './app-material/app-material.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MdePopoverModule } from '@material-extended/mde';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ResumeDialogComponent } from './resume-dialog/resume-dialog.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'notfoundpage',
    pathMatch: 'full'
  },
  {
    path: 'main',
    component: AppComponent,
    data: { title: 'Planeador' }
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ResumeDialogComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'reload', relativeLinkResolution: 'legacy' }),
    BrowserModule,
    AppRoutingModule,
    AppMaterialModule,
    NgxDatatableModule,
    BrowserAnimationsModule,
    MdePopoverModule
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent],
})

export class AppModule { }
